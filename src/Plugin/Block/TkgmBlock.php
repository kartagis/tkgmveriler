<?php

namespace Drupal\tkgmveriler\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Bir tkgmveriler bloğu sağlar
 *
 * @Block (
 *  id="tkgmveriler_block",
 *  admin_label=@Translation("tkgmveriler"),
 *  category=@Translation("tkgmveriler"),
 * )
 */
 class TkgmBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */

   public function build() {
     $block = [];
     $block['content'] = [
       '#markup' => tkgm_get_data(),
       '#attributes' => ['class' => 'tkgm-block'],
       '#attached' => [
         'library' => 'tkgmveriler/tkgmveriler'
       ]
         ];
     return $block;
   }
 }
